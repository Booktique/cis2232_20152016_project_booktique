package info.hccis.booktique.util;

import info.hccis.booktique.model.jpa.Book;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @since September 26, 2015
 * @author Chris MacEachern
 */
public class FileUtility {

    /**
     * the createFile() method accepts a String for the file path. The Files
     * createDirectores() creates the directory if it doesn't exist and the
     * createFile() method creates the file.
     *
     * @param file
     */
    public static void createFile(String file) {

        Path path = Paths.get(file);

        try {
            Files.createDirectories(path.getParent());
            Files.createFile(path);
        } catch (FileAlreadyExistsException e) {
            System.err.println("Note that the campers file already exists: ");
        } catch (IOException ex) {
            System.err.println("File could not be created: ");
        }
    }

    /**
     * The deleteFile() method accepts a String for the file path. The Files
     * delete() method is used to delete the file passed in.
     *
     * @param file
     */
    public static void deleteFile(String file) {

        try {
            Path path = Paths.get(file);
            Files.delete(path);
        } catch (IOException ex) {
            System.err.println("File could not be deleted: ");
        }
    }

    /**
     * The Write() method accepts a String for the file path and a String for
     * the content to write to the file. A FileWriter object is created that
     * will append to the file and is passed to the PrintWriter constructor. The
     * PrintWriter object uses its append() method to write the content to the
     * file.
     *
     * @param file
     * @param content
     */
    public static void Write(String file, String content) {

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file, true);
        } catch (IOException ex) {
            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }

        PrintWriter printWriter = new PrintWriter(fileWriter);
        try {
            printWriter.append(content + System.lineSeparator());
            printWriter.close();
        } catch (Exception e) {
            System.err.println("Issue occured writing to file.");
        }

    }

    /**
     * The readFile() method accepts a String for the file path and an ArrayList
     * of Deliverable objects. A BufferedReader object is created and is used to
     * read lines of the file that was passed in. The BufferedReader splits the
     * lines to create a new Deliverable object and adds to the ArrayList which
     * is returned.
     *
     * @param file
     * @param deliverables
     * @return
     * @throws IOException
     */
//    public static ArrayList<Book> readFile(String file, ArrayList<Book> books) throws IOException {
//
//        BufferedReader br = null;
//        try {
//            br = Files.newBufferedReader(Paths.get(file));
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        String line = br.readLine();
//        while (line != null) {
//            books.add(new Book(line.split(",")));
//            line = br.readLine();
//        }
//        br.close();
//
//        return books;
//    }

    /**
     * This method uses the Files exists() method to return a boolean indicating
     * whether the file exists
     *
     * @param file
     * @return
     */
    public static boolean fileExists(String file) {

        Path path = Paths.get(file);
        return Files.exists(path);
    }
}
