package info.hccis.booktique.web.services;

import info.hccis.booktique.data.springdatajpa.BookRepository;
import info.hccis.booktique.data.springdatajpa.ProgramClassesRepository;
import info.hccis.booktique.data.springdatajpa.ProgramRepository;
import info.hccis.booktique.model.jpa.Book;
import info.hccis.booktique.model.jpa.Program;
import info.hccis.booktique.model.jpa.ProgramClasses;
import info.hccis.booktique.service.BookService;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.PUT;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
//import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Component
@Path("/dataaccess")
@Scope("request")
public class BooktiqueRestService {

    @Resource
    private final BookRepository br;
    private final ProgramRepository pr;
    private final ProgramClassesRepository pcr;
    private final BookService bookService;

    public BooktiqueRestService(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.br = applicationContext.getBean(BookRepository.class);
        this.pr = applicationContext.getBean(ProgramRepository.class);
        this.pcr = applicationContext.getBean(ProgramClassesRepository.class);
        this.bookService = applicationContext.getBean(BookService.class);
    }
    
    /**
     * Allows the application to addBooks to the database.
     * Parameters are in this order:
     *      Title, Edition, Author,Publisher, ISBN, ClassCode, price, inventory
     * 
     * @param userData
     * @return
     */    
    @GET
    @Path("addBook/{param}")
    public Response addBook(@PathParam("param") String userData){
        int responseCode;
        String message;
        String[] bookData = userData.split(",");
        
        if(bookData.length == 8){
            Book book = new Book(bookData[0], bookData[1], bookData[2], bookData[3], bookData[4], bookData[5], Float.parseFloat(bookData[6]), Integer.parseInt(bookData[7]));
            responseCode = 201;
            
            br.save(book);
            message = "Book was added."+"\nClassCode: "+bookData[5]+", Title: "+bookData[0]+", Author: "+bookData[2]+", ISBN: "+bookData[4]+
                    ", Publisher: "+bookData[3]+", Edition: "+bookData[1]+", Price: "+bookData[6]+", Inventory: "+bookData[7];
        } else {
            message = "Book was not added!";
            responseCode = 400;
        }
        
        return Response.status(responseCode).entity(message).build();
             
    }
    /**
     * 
     * @param userData
     * @return 
     */
    @GET
    @Path("updateBook/{param}")
    public Response updateBook(@PathParam("param") String userData){
        int responseCode;
        String message;
        String[] bookData = userData.split(",");
        
        if(bookData.length == 8){
            Book book = new Book(bookData[0], bookData[1], bookData[2], bookData[3], bookData[4], bookData[5], Float.parseFloat(bookData[6]), Integer.parseInt(bookData[7]));
            responseCode = 201;
            
            bookService.updateBook(book);
            message = "Book was updated."+"\nClassCode: "+bookData[5]+", Title: "+bookData[0]+", Author: "+bookData[2]+", ISBN: "+bookData[4]+
                    ", Publisher: "+bookData[3]+", Edition: "+bookData[1]+", Price: "+bookData[6]+", Inventory: "+bookData[7];
        } else {
            message = "Book was not updated!";
            responseCode = 400;
        }
        
        return Response.status(responseCode).entity(message).build();
             
    }

    /**
     * Allows application to get booking data from database. Data well be sent
     * in this order: bookingId,numberOfPassengers,cost,dateBooked,customerEmail
     *
     * @return
     */
    @GET
    @Path("/books")
    @Produces("application/json")
    public Response getBooks(){
        int responseCode;
        
        ArrayList<Book> books = (ArrayList<Book>) br.findAll();
        
        JSONObject json = new JSONObject();
        JSONObject object;
        JSONArray array = new JSONArray();
        try{
            if(!books.isEmpty()){
                responseCode = 200;
                for(Book aBook: books){
                    object = new JSONObject();
                    object.put("ISBN", aBook.getISBN());
                    object.put("classCode", aBook.getClassCode());
                    object.put("bookName", aBook.getBookName());
                    object.put("edition", aBook.getEdition());
                    object.put("author", aBook.getAuthor());
                    object.put("publisher", aBook.getPublisher());
                    object.put("price", aBook.getPrice());
                    object.put("inventory", aBook.getInventory());
                    array.put(object);
                }
                
                json.put("books", array);
            } else {
                responseCode = 204;
            }
        } catch(JSONException jse){
            responseCode = 501;
        }
        return Response.status(responseCode).entity(json.toString()).build();
    }
    
    @GET
    @Path("/programs")
    @Produces("application/json")
    public Response getPrograms(){
        int responseCode;
        
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        
        JSONObject json = new JSONObject();
        JSONObject object;
        JSONArray array = new JSONArray();
        try{
            if(!programs.isEmpty()){
                responseCode = 200;
                for(Program aProgram: programs){
                    object = new JSONObject();
                    object.put("programCode", aProgram.getProgramCode());
                    object.put("programName", aProgram.getProgramName());
                    array.put(object);
                }
                
                json.put("programs", array);
            } else {
                responseCode = 204;
            }
        } catch(JSONException jse){
            responseCode = 501;
        }
        return Response.status(responseCode).entity(json.toString()).build();
    }
    
    @GET
    @Path("/courses")
    @Produces("application/json")
    public Response getCourses(){
        int responseCode;
        
        ArrayList<ProgramClasses> courses = (ArrayList<ProgramClasses>) pcr.findAll();
        
        JSONObject json = new JSONObject();
        JSONObject object;
        JSONArray array = new JSONArray();
        try{
            if(!courses.isEmpty()){
                responseCode = 200;
                for(ProgramClasses aCourse: courses){
                    object = new JSONObject();
                    object.put("courseCode", aCourse.getClassCode());
                    object.put("programCode", aCourse.getProgramCode());
                   
                    array.put(object);
                }
                
                json.put("courses", array);
            } else {
                responseCode = 204;
            }
        } catch(JSONException jse){
            responseCode = 501;
        }
        return Response.status(responseCode).entity(json.toString()).build();
    }


}
