
package info.hccis.booktique.web;


import info.hccis.booktique.dao.BookDAO;
import info.hccis.booktique.data.springdatajpa.BookRepository;
import info.hccis.booktique.data.springdatajpa.CourseRepository;
import info.hccis.booktique.data.springdatajpa.ProgramClassesRepository;
import info.hccis.booktique.data.springdatajpa.ProgramRepository;
import info.hccis.booktique.model.DatabaseConnection;
import info.hccis.booktique.model.jpa.Book;
import info.hccis.booktique.model.jpa.Course;
import info.hccis.booktique.model.jpa.Program;
import info.hccis.booktique.model.jpa.ProgramClasses;
import info.hccis.booktique.model.jpa.UserAccess;
import info.hccis.booktique.service.BookService;
import info.hccis.booktique.util.FileUtility;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @since December 11, 2015
 * @author Chris MacEachern
 * @author Chelsea Ferguson
 * @author Alex Pelletier
 */

@Controller
public class BookController {

    
    private final BookRepository br;
    private final ProgramRepository pr;
    private final CourseRepository cr;
    private final ProgramClassesRepository pcr;
    

    @Autowired
    public BookController(BookRepository br, ProgramRepository pr, CourseRepository cr, ProgramClassesRepository pcr) {
        
        this.br = br;
        this.pr = pr;
        this.cr = cr;
        this.pcr = pcr;
       
    }

    /**
     * This method will get a list of books and add it to the model to be displayed on the books page
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("/booktique/books")
    public String showBooks(Model model, HttpSession session) {
        
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        //checking if user has a staff login
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        
        //getting an arraylist of books and adding to the model
        ArrayList<Book> books = (ArrayList<Book>) br.findAll();
        model.addAttribute("books", books);
        model.addAttribute("staffLogin", staffLogin);
        return "/booktique/books";
    }
    //-------------------------------------------------------------------------//
    /**
     * This method gets a list of programs and adds it to the model to be displayed on the program page
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("/booktique/program")
    public String showPrograms(Model model, HttpSession session) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        //checking if user has a staff login
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        //getting list of programs from database and adding them to the model
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        model.addAttribute("programs", programs);
        model.addAttribute("staffLogin", staffLogin);
        return "/booktique/program";
    }
    /**
     * This method will get a list of courses for the program that was chosen and display the books
     * needed for the course.
     * @param model
     * @param session
     * @param request
     * @return 
     */
    @RequestMapping("/booktique/courses")
    public String showCourses(Model model, HttpSession session, HttpServletRequest request) {
       //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        //checking if user has a staff login
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        //getting the program code that the user selected
        String programCode = request.getParameter("programCode");
        String programName = "";
        int test = 0;
        ArrayList<Program> programs = (ArrayList<Program>) pr.findByProgramCode(programCode);
        for(Program program : programs){
             programName = program.getProgramName();           
        }
        model.addAttribute("programName", programName);
        
        
        //creating empty Course arraylist and variable
        ArrayList<Course> courses = new ArrayList();
        Course aCourse;
        //getting list of all courses and a list of the courses for the program selected
        ArrayList<Course> allCourses = (ArrayList<Course>) cr.findAll();
        ArrayList<ProgramClasses> selectedCourses = (ArrayList<ProgramClasses>) pcr.findByProgramCode(programCode);
        
        for(ProgramClasses programclass: selectedCourses){
            System.out.println(programclass.toString());
        }
        //empty arraylist to hold books
        ArrayList<Book> books = new ArrayList();
        //looping through the selected courses
        for(ProgramClasses selected: selectedCourses){
            //looping through all courses withing the selected loop
            for(Course theCourse: allCourses){
                //checking if class code matches
                if(theCourse.getClassCode().equalsIgnoreCase(selected.getClassCode())){
                    //if a match is found then all books for that course code are found
                    books = (ArrayList<Book>) br.findByClassCode(selected.getClassCode());
                    //setting up the course object and adding it to the courses arraylist
                    aCourse = new Course();
                    aCourse.setClassCode(theCourse.getClassCode());
                    aCourse.setClassName(theCourse.getClassName());
                    aCourse.setProgramCode(theCourse.getProgramCode());
                    aCourse.setYear(theCourse.getYear());
                    aCourse.setBooks(books);
                    courses.add(aCourse);
                }
            }
        }
        //adding the courses arraylist to the model to be displayed
        model.addAttribute("courses", courses);
        model.addAttribute("staffLogin", staffLogin);
        return "/booktique/courses";
    }
    //-------------------------------------------------------------------------//
    /**
     * This method will pre populate the book edit form with the information of the book selected
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/BookEdit")
    public String editBook(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("book") Book book, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting ISBN of book selected
        String ISBN = request.getParameter("ISBN");
        
        System.out.println("ISBN-->"+ISBN);
        //getting a list of all books
        ArrayList<Book> books = (ArrayList<Book>) br.findAll();
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        //looping through book list and if ISBN matches then the book object is setup
        for(Book aBook: books){
            if(ISBN.equalsIgnoreCase(aBook.getISBN())){
                book = aBook;
            }
        }
        boolean bookEdited = false;
        //adding book to the model
        model.addAttribute("bookEdited", bookEdited);
        model.addAttribute("book", book);
        model.addAttribute("courses", courses);
        return "/booktique/BookEdit";
    }
    
    /**
     * This method will process the book edit request.  If no error are found then the book is updated via
     * JPA
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/processbookedit")
    public String processEdit(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("book") Book book, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        boolean bookEdited = false;
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        //if no errors are found then the database is updated
        if(!result.hasErrors()){
            br.save(book);
            bookEdited = true;
        }
        model.addAttribute("bookEdited", bookEdited);
        model.addAttribute("courses", courses);
        //model.addAttribute("book", book);
        return "/booktique/BookEdit";
    }
    @RequestMapping("/booktique/programEdit")
    public String editProgram(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("program") Program program, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting ISBN of book selected
        String programCode = request.getParameter("programCode");
        
        System.out.println("programCode-->"+programCode);
        //getting a list of all books
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        //looping through book list and if ISBN matches then the book object is setup
        for(Program aProgram: programs){
            if(programCode.equalsIgnoreCase(aProgram.getProgramCode())){
                program = aProgram;
            }
        }
        //adding book to the model
        model.addAttribute("program", program);
        return "/booktique/programEdit";
    }
    
    /**
     * This method will process the book edit request.  If no error are found then the book is updated via
     * JPA
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/processprogramedit")
    public String processProgramEdit(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("program") Program program, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        //getting the information that the user entered
        String programName = request.getParameter("programName");
        String programCode = request.getParameter("programCode"); 
        
        //getting a list of books
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        //looping through the list of books and if ISBN matches then the book is setup
        for(Program aProgram: programs){
            if(programCode.equalsIgnoreCase(aProgram.getProgramCode())){
                program = aProgram;
            }
        }
        //setting the book information to what the user entered
        program.setProgramName(programName);
        program.setProgramCode(programCode);
        //if no errors are found then the database is updated
        boolean programEdited = false;
        if(!result.hasErrors()){
            pr.save(program);
            programEdited = true;
        }
        model.addAttribute("programEdited", programEdited);
        //model.addAttribute("book", book);
        return "/booktique/programEdit";
    }
    @RequestMapping("/booktique/courseEdit")
    public String editBook(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("course") Course course, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting ISBN of book selected
        String classCode = request.getParameter("classCode");
        
        System.out.println("ClassCode-->"+classCode);
        //getting a list of all books
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        //looping through book list and if ISBN matches then the book object is setup
        for(Course aCourse: courses){
            if(classCode.equalsIgnoreCase(aCourse.getClassCode())){
                course = aCourse;
            }
        }
        //adding book to the model
        model.addAttribute("course", course);
        return "/booktique/courseEdit";
    }
    
    /**
     * This method will process the book edit request.  If no error are found then the book is updated via
     * JPA
     * @param model
     * @param session
     * @param request
     * @param course
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/processcourseedit")
    public String processCourseEdit(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("course") Course course, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        //getting the information that the user entered
        String classCode = request.getParameter("classCode");
        
        //getting a list of books
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        //looping through the list of books and if ISBN matches then the book is setup
        for(Course aCourse: courses){
            if(classCode.equalsIgnoreCase(aCourse.getClassCode())){
                course = aCourse;
            }
        }

        
        //if no errors are found then the database is updated
        boolean courseEdited = false;
        if(result.hasErrors()){
            System.out.println("Error-->"+result.getAllErrors());
        }
        if(!result.hasErrors()){
            cr.save(course);
            courseEdited = true;
        }
        model.addAttribute("courseEdited", courseEdited);
        //model.addAttribute("book", book);
        return "/booktique/courseEdit";
    }
    /**
     * This method will allow the user to add a book.
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/AddBook")
    public String addBook(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting a list of courses to populated the courseCode drop down list
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        Book book = new Book();
        boolean bookAdded = false;
        model.addAttribute("bookAdded", bookAdded);
        model.addAttribute("book", book);
        model.addAttribute("courses", courses);
        return "/booktique/AddBook";
    }
    /**
     * This method will process the add book request.  If no errors are found then the book is added via
     * JPA
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/processaddbook")
    public String processingAddBook(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("book") Book book, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        boolean bookAdded = false;
        //getting list of courses for courseCode dropDown
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        model.addAttribute("courses", courses);

        //if no errors are found then the book is added to the database
        if(!result.hasErrors()){
            
            br.save(book);
            bookAdded = true;
        }
        model.addAttribute("bookAdded", bookAdded);
        return "/booktique/AddBook";
    }
    @RequestMapping("/booktique/addProgram")
    public String addProgram(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting a list of courses to populated the courseCode drop down list
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        Program program = new Program();
        model.addAttribute("program", program);
        model.addAttribute("programs", programs);
        return "/booktique/addProgram";
    }
    /**
     * This method will process the add book request.  If no errors are found then the book is added via
     * JPA
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/processAddProgram")
    public String processingAddProgram(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("program") Program program, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        //getting list of courses for courseCode dropDown
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        model.addAttribute("programs", programs);
        
        //if no errors are found then the book is added to the database
        boolean programAdded = false;
        if(!result.hasErrors()){
            pr.save(program);
            programAdded = true;
        }
        model.addAttribute("programAdded", programAdded);
        return "/booktique/addProgram";
    }
    @RequestMapping("/booktique/addCourse")
    public String addCourse(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        Course course = new Course();
        model.addAttribute("course", course);
        model.addAttribute("programs", programs);
        
        //display program title
        String programName = request.getParameter("programName");
        model.addAttribute("programName", programName);
        
        
        ArrayList<Program> forCode = (ArrayList<Program>) pr.findByProgramName(programName);
        String programCode ="TEST";
        for(Program program : forCode){
            programCode = program.getProgramCode();
        }
        model.addAttribute("programCode", programCode);
        return "/booktique/addCourse";
    }
    /**
     * This method will process the add book request.  If no errors are found then the book is added via
     * JPA
     * @param model
     * @param session
     * @param request
     * @param book
     * @param result
     * @return 
     */
    @RequestMapping("/booktique/processaddcourse")
    public String processingaddcourse(Model model, HttpSession session, HttpServletRequest request, @Valid @ModelAttribute("course") Course course, BindingResult result) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        //getting list of courses for courseCode dropDown
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        model.addAttribute("programs", programs);
        //getting the information the user entered from the form
        String classCode = request.getParameter("classCode");
        String programCode = request.getParameter("programCode");
        
        //if no errors are found then the book is added to the database
        ProgramClasses aCourse = new ProgramClasses();
        aCourse.setClassCode(classCode);
        aCourse.setProgramCode(programCode);
        boolean courseAdded = false;
        if(!result.hasErrors()){
            cr.save(course);
            pcr.save(aCourse);
            courseAdded = true;
        }
        model.addAttribute("courseAdded", courseAdded);
        return "/booktique/addCourse";
    }
    /**
     * This method will process the users request to delete a book
     * @param model
     * @param session
     * @param request
     * @return 
     */
    @RequestMapping("/booktique/DeleteBook")
    public String deleteBook(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting the ISBN of the book that was selected
        String ISBN = request.getParameter("ISBN");
        
        System.out.println("ISBN-->"+ISBN);
        //getting a list of all books
        ArrayList<Book> books = (ArrayList<Book>) br.findAll();
        Book book = new Book();
        //looping through books and if ISBN matches then the book object is setup
        for(Book aBook: books){
            if(ISBN.equalsIgnoreCase(aBook.getISBN())){
                book = aBook;
            }
        }
        //deleting the selected book and repopulating the books list to reflect the changes made
        br.delete(book);
        books = (ArrayList<Book>) br.findAll();
        //adding list to the model and login status
        model.addAttribute("books", books);
        model.addAttribute("staffLogin", staffLogin);
        return "/booktique/books";
    }
    @RequestMapping("/booktique/DeleteProgram")
    public String deleteProgram(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting the ISBN of the book that was selected
        String programCode = request.getParameter("programCode");
        
        System.out.println("programCode-->"+programCode);
        //getting a list of all books
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        Program program = new Program();
        //looping through books and if ISBN matches then the book object is setup
        for(Program aProgram: programs){
            if(programCode.equalsIgnoreCase(aProgram.getProgramCode())){
                program = aProgram;
            }
        }
        //deleting the selected book and repopulating the books list to reflect the changes made
        pr.delete(program);
        programs = (ArrayList<Program>) pr.findAll();
        //adding list to the model and login status
        model.addAttribute("program", program);
        model.addAttribute("staffLogin", staffLogin);
        return "/booktique/program";
    }
    @RequestMapping("/booktique/DeleteCourse")
    public String deleteCourse(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        //getting the ISBN of the book that was selected
        String classCode = request.getParameter("classCode");
        
        System.out.println("classCode-->"+classCode);
        //getting a list of all books
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        Course course = new Course();
        //looping through books and if ISBN matches then the book object is setup
        for(Course aCourse: courses){
            if(classCode.equalsIgnoreCase(aCourse.getClassCode())){
                course = aCourse;
            }
        }
        //deleting the selected book and repopulating the books list to reflect the changes made
        cr.delete(course);
        courses = (ArrayList<Course>) cr.findAll();
        //adding list to the model and login status
        model.addAttribute("courses", courses);
        model.addAttribute("staffLogin", staffLogin);
        return "/booktique/courses";
    }

    //-------------------------------------------------------------------------//
    //Report Page(s) controllers
    /**
     * GenerateReport.html - Main page to display options for generating a report.
     * 
     * @param model
     * @param session
     * @param dbConnectionIn
     * @return 
     */
    @RequestMapping("/booktique/generateReport")
    public String reportPage(Model model, HttpSession session, DatabaseConnection dbConnectionIn){     
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        
        
        //get list of programs
        ArrayList<Program> programs = (ArrayList<Program>) pr.findAll();
        model.addAttribute("programs", programs);
        
        //get list of courses
        ArrayList<Course> courses = (ArrayList<Course>) cr.findAll();
        model.addAttribute("courses", courses);
        return "/booktique/generateReport";
    }
    //Process Stock report option
    /**
     * showReport.html - Controller gets the chosen report type from the parameters
     * of the generateReport.html page and connects to the database to return results
     * based on report option.
     * 
     * @param model
     * @param session
     * @param request
     * @param book
     * @return 
     */
    @RequestMapping("/booktique/showReport")
    public String processStockReport(Model model, HttpSession session, HttpServletRequest request, Book book){
       DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
     
       String reportTitle = "";
       int bookTotalFound = 0;
       String reportChosen = request.getParameter("report");
        ArrayList<Book> books = new ArrayList();
        
        switch (reportChosen) {
            case "inStock":
               //list of all books
               ArrayList<Book> allBooks = (ArrayList<Book>) br.findAll();
               //iterate through books
               for(Book theBook: allBooks){
                   //find where inventory is above 0
                   if(theBook.getInventory() > 0){
                       books.add(theBook);
                       bookTotalFound++; 
                   }
                    
                }
                model.addAttribute("books", books);
                model.addAttribute("bookTotalFound", bookTotalFound);

                //report title
                reportTitle = "In Stock";
                model.addAttribute("reportTitle", reportTitle);
                break;
    
            case "outOfStock":
               //select all books with inventory of 0
               ArrayList<Book> bookList = (ArrayList<Book>) br.findByInventory(0);
               //iterate through books to get count
               for(Book theBook: bookList){
                       bookTotalFound++;                 
                }
               
               //Return books found
                model.addAttribute("books", bookList);
                model.addAttribute("bookTotalFound", bookTotalFound);
                //report title
                reportTitle = "Out of Stock";
                model.addAttribute("reportTitle", reportTitle);
                break;
                
            default:
                reportTitle = "No report type chosen";
                model.addAttribute("reportTitle", reportTitle);
                break;
        }
        
        
        return "/booktique/showReport";
    }
    //returns books based on chosen program
    @RequestMapping("/booktique/showReportbyProgram")
    public String processProgramReport(Model model, HttpSession session, HttpServletRequest request, Book book){
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
//        ArrayList<Book> byProgram  = BookDAO.booksByProgram(databaseConnection, book);
//        model.addAttribute("books", byProgram);
                    
        int bookTotalFound = 0;
        //JPA -
        ArrayList<Book> byProgram = new ArrayList();
        
        String programChosen = request.getParameter("programName");
        model.addAttribute("programCode", programChosen);
        ArrayList<ProgramClasses> progClass = (ArrayList<ProgramClasses>) pcr.findByProgramCode(programChosen);
       
        for(ProgramClasses aProg : progClass){
            String courseCode = aProg.getClassCode();
            
            ArrayList<Book> findBook = (ArrayList<Book>) br.findByClassCode(courseCode);
            for(Book aBook : findBook){
               byProgram.add(aBook);
               bookTotalFound++;
            }        
        }

        //Return books found
        model.addAttribute("books", byProgram);
        model.addAttribute("bookTotalFound", bookTotalFound);
        //Report Title
        String reportTitle = "Books by Program";
        model.addAttribute("reportTitle", reportTitle);
       return "/booktique/showReport";
    }
    //returns books with a chosen course
    @RequestMapping("/booktique/showReportbyCourse")
    public String processCourseReport(Model model, HttpSession session, HttpServletRequest request, Book book){
        DatabaseConnection databaseConnection = (DatabaseConnection) session.getAttribute("db");
      
        int bookTotalFound = 0;
        //JPA
        //getting the class code from the paramters
         String classCode = request.getParameter("classCode");
         model.addAttribute("classCode", classCode);
         //empty array list
         ArrayList<Book> allCourses = (ArrayList<Book>) br.findByClassCode(classCode);
         //iterate through books found to get total count
         for(Book theBook: allCourses){
            bookTotalFound++; 
         }
         
        //Return books found
             model.addAttribute("books", allCourses);
             model.addAttribute("bookTotalFound", bookTotalFound);

        //Report Title
        String reportTitle = "Books by Course";
        model.addAttribute("reportTitle", reportTitle);
       return "/booktique/showReport";
    }
    
    @RequestMapping("/booktique/filewrite")
    public String fileWrite(Model model, HttpSession session, HttpServletRequest request) {
        //checking if user is logged in
        UserAccess login = (UserAccess) session.getAttribute("userToken");
        int userTypeCode = login.getUserTypeCode();
        
        boolean staffLogin = false;
        if(userTypeCode==0){
            model.addAttribute("login", login);
            return "/other/login";
        }
        else if(userTypeCode==1){
            staffLogin = true;
        }
        else if(userTypeCode==2){
            return "/other/restricted";
        }
        
        final String FILE = "\\Books\\booklist.txt";
        boolean errorOccured = false;
        if(FileUtility.fileExists(FILE)){
            FileUtility.deleteFile(FILE);
        } 
        
        FileUtility.createFile(FILE);
        
        ArrayList<Book> books = (ArrayList<Book>) br.findAll();
        for(Book aBook: books){
            FileUtility.Write(FILE, aBook.getBookName());
        }
        model.addAttribute("errorOccured", errorOccured);
        return "/booktique/filewrite";
    }
}
