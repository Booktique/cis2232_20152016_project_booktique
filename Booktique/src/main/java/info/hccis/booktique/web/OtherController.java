package info.hccis.booktique.web;

import info.hccis.booktique.data.springdatajpa.BookRepository;
import info.hccis.booktique.model.DatabaseConnection;
import info.hccis.booktique.model.jpa.Book;
import info.hccis.booktique.model.jpa.UserAccess;
import info.hccis.booktique.util.Utility;
import java.util.ArrayList;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @since December 11, 2015
 * @author Chris MacEachern
 * @author Chelsea Ferguson
 * @author Alex Pelletier
 */

@Controller
public class OtherController {

    private final BookRepository br;
    
    @Autowired
    public OtherController(BookRepository br){
        this.br = br;
    }
    
    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }

    @RequestMapping(value="/other/login",method=RequestMethod.POST)
    public String login(Model model, HttpSession session,@ModelAttribute("login") UserAccess login) {
        /*
        Hit the web service to see if valid credentials.
        */
        //String urlString = "http://bjmac.hccis.info:8080/cisadmin/rest/useraccess/bjmac_bookstoreDatabase," + login.getUsername() + "," + login.getPassword();
//        String urlString = "http://localhost:8080/cisadmin/rest/useraccess/cis2232_booktique," + login.getUsername() + "," + login.getPassword();
        String urlString = "http://cis2232.hccis.info:8080/cisadmin/rest/useraccess/cis2232_booktique," + login.getUsername() + "," + login.getPassword();
        System.out.println("url for validate=" + urlString);
        String result = Utility.getResponseFromRest(urlString);

        /*
        if successful add token and return to welcome.  Otherwise back to the login.  
        */
        
        if (!result.equals("0")) {
            login.setUserTypeCode(Integer.parseInt(result));
            session.setAttribute("userToken", login);
            ArrayList<Book> books = (ArrayList<Book>) br.findAll();
            boolean staffLogin = false;
            if(login.getUserTypeCode()==1){
                staffLogin = true;
            }
            model.addAttribute("books", books);
            model.addAttribute("staffLogin", staffLogin);
            return "booktique/books";
        } else {
            login = new UserAccess();
            model.addAttribute("message","Login failed");
            model.addAttribute("login", login);
            return "other/login";
        }
    }
    
    @RequestMapping("/")
    public String showHome(Model model, HttpSession session) {
        System.out.println("in controller for /");
        UserAccess login = new UserAccess();
        session.setAttribute("userToken", login);
        model.addAttribute("login", login);
      
        return "other/login";
    }
    
    @RequestMapping("/other/restricted")
    public String restricted(Model model, HttpSession session) {
        
        return "other/restricted";
    }

}
