/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.booktique.service;

import info.hccis.booktique.model.jpa.Book;

/**
 *
 * @author Chris
 * @author Chelsea
 */
public interface BookService {

    public abstract String updateBook(Book aBook);
    
    public abstract String saveBook(Book aBook);
}
