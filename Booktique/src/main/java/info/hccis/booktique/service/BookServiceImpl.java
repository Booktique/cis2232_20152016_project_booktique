
package info.hccis.booktique.service;

import info.hccis.booktique.dao.BookDAO;
import info.hccis.booktique.data.springdatajpa.BookRepository;
import info.hccis.booktique.model.jpa.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Chris
 * @author Chelsea
 */
@Service
public class BookServiceImpl implements BookService{
    
    private final BookRepository bookRepository;
    
    @Autowired
    public BookServiceImpl(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

 
    @Override
    public String saveBook(Book aBook){
        String message;
        try{
            bookRepository.save(aBook);
            message = "Book was added";
        } catch (Exception ex){
            message = "Book was not added!";
        }
        return message;
    }
    
    @Override
    public String updateBook(Book aBook) {
        String message;
        try{
            BookDAO.updateBook(aBook);
            bookRepository.save(aBook);
            message = "Book was updated.";
        } catch (Exception ex){
            message = "Book was not updated!";
        }
        return message;
    }


    
}
