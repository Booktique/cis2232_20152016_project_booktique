
package info.hccis.booktique.dao;

import info.hccis.booktique.dao.util.ConnectionUtils;
import info.hccis.booktique.dao.util.DbUtils;
import info.hccis.booktique.data.springdatajpa.BookRepository;
import info.hccis.booktique.model.DatabaseConnection;
import info.hccis.booktique.model.jpa.Book;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Chris
 * @author Chelsea
 */
public class BookDAO {
    
    
    /**
     * Method to update the book based on the book object passed in the parameters.
     * The book will update based on where the input ISBN matches an ISBN in the 
     * database.
     * 
     * @author Chelsea
     * @param aBook 
     */
    public static void updateBook(Book aBook){
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtils.getConnection();
            
            sql = "";
            sql = "UPDATE Books ";
            sql += "SET ISBN='"+aBook.getISBN()+"', "+"author ='"+aBook.getAuthor()+"', "+"inventory ="+aBook.getInventory()+", "+"edition ='"+aBook.getEdition()+"', "+"price ="+aBook.getPrice()
                        +", "+"publisher ='"+aBook.getPublisher()+"', "+"bookName ='"+aBook.getBookName();
            sql += " WHERE ISBN='"+aBook.getISBN()+"'";
            
            ps = conn.prepareStatement(sql);
            ps.executeUpdate();

            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("Error Message------------>"+errorMessage);
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
     
    }
    /**
     * This method returns total of books in stock(inventory of at least 1)
     * 
     * @author Chelsea
     * @param databaseConnection
     * @return 
     * @since 2015-15-08
     */
    public static ArrayList<Book> inStockBook(DatabaseConnection databaseConnection){
        ArrayList<Book> bookList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtils.getConnection();
            
            sql = "SELECT * FROM books WHERE inventory > 0 ORDER BY inventory ASC";        
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String title = rs.getString("bookName");
                String author = rs.getString("author");
                String edition = rs.getString("edition");
                String publisher = rs.getString("publisher");
                String isbn  = rs.getString("ISBN");
                String classCode = rs.getString("classCode");
                float price = rs.getFloat("price");
                int inventory = rs.getInt("inventory");
                
                //note- error here
//               Book newBook = new Book(title, edition, author, publisher, isbn, classCode, price, inventory);
              Book newBook = new Book();
                bookList.add(newBook);
            }
            ps.executeUpdate();
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("Error Message------------>"+errorMessage);
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        
        return bookList;
    }
    /**
     * This method returns total of books out of stock(inventory of 0)
     * 
     * @author Chelsea
     * @param databaseConnection
     * @return 
     * @since 2015-15-08
     **/
    public static ArrayList<Book> outOfStockBook(DatabaseConnection databaseConnection){
        ArrayList<Book> bookList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtils.getConnection();
            
            sql = "SELECT * FROM books WHERE inventory = 0 ORDER BY title ASC";        
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String title = rs.getString("bookName");
                String author = rs.getString("author");
                String edition = rs.getString("edition");
                String publisher = rs.getString("publisher");
                String isbn  = rs.getString("ISBN");
                String classCode = rs.getString("classCode");
                float price = rs.getFloat("price");
                int inventory = rs.getInt("inventory");
                
              Book  newBook = new Book();  
//                Book newBook = new Book(title, edition, author, publisher, isbn, classCode, price, inventory);
                bookList.add(newBook);
            }
            ps.executeUpdate();
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("Error Message------------>"+errorMessage);
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        
        return bookList;
    }
    /**
     * This method returns the books that have are for the program chosen by the user.
     * 
     * @author Chelsea
     * @param databaseConnection
     * @return 
     * @since 2015-15-08
     **/
    public static ArrayList<Book> booksByProgram(DatabaseConnection databaseConnection, Book book){
        ArrayList<Book> bookList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtils.getConnection();
            sql = "SELECT * FROM `books` WHERE classCode IN (SELECT classCode FROM programclasses WHERE programCode = '"+book.getClassCode()+"') ORDER BY title ASC";        
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String title = rs.getString("bookName");
                String author = rs.getString("author");
                String edition = rs.getString("edition");
                String publisher = rs.getString("publisher");
                String isbn  = rs.getString("ISBN");
                String classCode = rs.getString("classCode");
                float price = rs.getFloat("price");
                int inventory = rs.getInt("inventory");
                
              Book  newBook = new Book();
              //There is an error when passing the information into the parameters.
//                Book newBook = new Book(title, edition, author, publisher, isbn, classCode, price, inventory);
                bookList.add(newBook);
            }
            ps.executeUpdate();
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("Error Message------------>"+errorMessage);
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        
        return bookList;
    }
        /**
     * This method returns the books that have are for the course chosen by the user.
     * 
     * @author Chelsea
     * @param databaseConnection
     * @return 
     * @since 2015-15-08
     **/
    public static ArrayList<Book> booksByCourse(DatabaseConnection databaseConnection, Book book){
        ArrayList<Book> bookList = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        
        try {
            conn = ConnectionUtils.getConnection();
            
            sql = "SELECT * FROM books WHERE classCode = "+book.getClassCode()+" ORDER BY title ASC";        
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                String title = rs.getString("bookName");
                String author = rs.getString("author");
                String edition = rs.getString("edition");
                String publisher = rs.getString("publisher");
                String isbn  = rs.getString("ISBN");
                String classCode = rs.getString("classCode");
                float price = rs.getFloat("price");
                int inventory = rs.getInt("inventory");
                
              Book  newBook = new Book();  
//                Book newBook = new Book(title, edition, author, publisher, isbn, classCode, price, inventory);
                bookList.add(newBook);
            }
            ps.executeUpdate();
            
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("Error Message------------>"+errorMessage);
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        
        return bookList;
    }
}
