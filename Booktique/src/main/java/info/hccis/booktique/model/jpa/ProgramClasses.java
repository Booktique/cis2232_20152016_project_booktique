
package info.hccis.booktique.model.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @since December 11, 2015
 * @author Chris MacEachern
 * @author Chelsea Ferguson
 * @author Alex Pelletier
 */
@Entity
@Table(name = "programclasses")
public class ProgramClasses {
    
    @Id
    private int id;
    @NotEmpty(message="Required")
    private String classCode;
    @NotEmpty(message="Required")
    private String programCode;
    
    public ProgramClasses(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    @Override
    public String toString() {
        return "ProgramClasses{" + "id=" + id + ", classCode=" + classCode + ", programCode=" + programCode + '}';
    }

    
    
}
