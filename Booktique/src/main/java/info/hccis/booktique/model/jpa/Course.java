
package info.hccis.booktique.model.jpa;

import java.util.ArrayList;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @since December 11, 2015
 * @author Chris MacEachern
 * @author Chelsea Ferguson
 * @author Alex Pelletier
 */

@Entity
@Table(name = "classes")
public class Course {
    
    @Id
    @NotEmpty(message="Required")
    private String classCode;
    @NotEmpty(message="Required")
    private String programCode;
    @NotEmpty(message="Required")
    private String className;
    @Min(0)
    @Max(2)
    private int year;
    @Transient
    ArrayList<Book> books;
    
    public Course(){
       
    }

    public ArrayList<Book> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<Book> books) {
        this.books = books;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return "Classes{" + "classCode=" + classCode + ", programCode=" + programCode + ", className=" + className + ", year=" + year + '}';
    }
    
    
}
