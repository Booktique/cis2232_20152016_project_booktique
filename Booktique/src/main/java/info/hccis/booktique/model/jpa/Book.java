
package info.hccis.booktique.model.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @since December 11, 2015
 * @author Chris MacEachern
 * @author Chelsea Ferguson
 * @author Alex Pelletier
 */
@Entity
@Table(name = "books")

public class Book {
    
    @Id
    @NotEmpty(message="Required")
    private String ISBN;
  
    private String classCode;
    @NotEmpty(message="Required")
    private String bookName;
    @NotEmpty(message="Required")
    private String edition;
    @NotEmpty(message="Required")
    private String author;
    @NotEmpty(message="Required")
    private String publisher;
    @Min(0)
    private float price;
    @Min(0)
    @Max(100)
    private int inventory;
    
    
    public Book(){
       
    }
    public Book(String bookName,String edition, String author, String publisher, String ISBN, String classCode, float price, int inventory){
        this.bookName = bookName;
        this.edition  = edition;
        this.author = author;
        this.publisher = publisher;
        this.ISBN = ISBN;
        this.classCode = classCode;
        this.price = price;
        this.inventory = inventory;
    }
    //getters and setters
    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }
    //toString
    @Override
    public String toString() {
        return "Book{" + "ISBN=" + ISBN + ", classCode=" + classCode + ", bookName=" + bookName + ", edition=" + edition + ", author=" + author + ", publisher=" + publisher + ", price=" + price + ", inventory=" + inventory + '}';
    }
    
    
}
