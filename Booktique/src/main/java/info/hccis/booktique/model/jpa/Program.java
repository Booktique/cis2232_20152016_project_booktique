
package info.hccis.booktique.model.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.validator.constraints.NotEmpty;


/**
 * @since December 11, 2015
 * @author Chris MacEachern
 * @author Chelsea Ferguson
 * @author Alex Pelletier
 */

@Entity
@Table(name = "programs")
public class Program {
    
    @Id
    @NotEmpty(message="Required")
    private String programCode;
    @NotEmpty(message="Required")
    private String programName;
    
    public Program(){
       
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @Override
    public String toString() {
        return "Program{" + "programCode=" + programCode + ", programName=" + programName + '}';
    }
    
    
}
