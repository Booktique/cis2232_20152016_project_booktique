/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.booktique.data.springdatajpa;

import info.hccis.booktique.model.jpa.Book;
import info.hccis.booktique.model.jpa.Course;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Chris
 */
@Repository
public interface CourseRepository extends CrudRepository<Course, Integer>{
    
}
