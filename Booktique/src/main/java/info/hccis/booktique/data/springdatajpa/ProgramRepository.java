/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.booktique.data.springdatajpa;


import info.hccis.booktique.model.jpa.Program;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmaceachern5567
 */
@Repository
public interface ProgramRepository extends CrudRepository<Program, Integer>{
    List<Program> findByProgramCode(String programCode);
    List<Program> findByProgramName(String programName);
}
