
package info.hccis.booktique.data.springdatajpa;

import info.hccis.booktique.model.jpa.Course;
import info.hccis.booktique.model.jpa.ProgramClasses;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Chris
 */
public interface ProgramClassesRepository extends CrudRepository<ProgramClasses, Integer>{
    List<ProgramClasses> findByProgramCode(String programCode);
}
