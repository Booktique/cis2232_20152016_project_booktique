CREATE DATABASE cis2232_booktique;

use cis2232_booktique;

CREATE TABLE IF NOT EXISTS `books` (
  `classCode` varchar(10) DEFAULT NULL,
  `bookName` varchar(100) DEFAULT NULL,
  `edition` varchar(20) DEFAULT NULL,
  `ISBN` varchar(40) NOT NULL DEFAULT '',
  `author` varchar(20) DEFAULT NULL,
  `publisher` varchar(20) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `inventory` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`classCode`, `bookName`, `edition`, `ISBN`, `author`, `publisher`, `price`, `inventory`) VALUES
('BUSI-1000', 'Fundamentals of Canadian Business Law wi', '2nd', '9780071050401', 'Willes', 'McGraw', 158.5, 2),
('CHEM-1000', 'Chemistry: A Molecular Approach - Canadi', '1st', '9780133070521', 'Tro/Fridgen', 'Pearson', 182.5, 7),
('MATH-2000', 'Basic Technology Mathematics with Calcul', '10th', '9780135067123', 'Allyn J. Washington', 'Pearson', 172.5, 1),
('ACCT-2005', 'Learning Sage 50 Accounting, A Modular A', '2014', '9780176531102', 'Freedman & Smith', 'Nelson', 0, 0),
('BIOS-2300', 'Introduction to Biotechnology', '3rd', '9780321766113', 'Thieman and Palladin', 'Login', 179.5, 10),
('BIOL-1300', 'Microbiology- Includes Mastering Microbi', '11th', '9780321767387', 'Tortora et al.', 'Login', 192.5, 8),
('ENGL-1000', 'English Simplified', '10th', '9780321996794', 'Ellsworth, Higgins', 'Pearson', 0, 0),
('BIOS-1100', 'Plant Biology', '2nd', '9780415356435', 'Lack and Evans', 'Login', 55, 13),
('CHEM-1300', 'BIOS Instant Notes: Biochemistry', '4th', '9780415608459', 'Hames and Hooper', 'Login', 54, 27),
('BIOL-2310', 'BIOS Instant Notes: Molecular Biology', '4th', '9780415684163', 'McLennan et al.', 'Login', 54, 0),
('BIOS-2310', 'Fundamental Labatory Approches for Bioch', '2nd', '9780470087664', 'Ninfa,Ballou and Ben', 'Wiley', 93.5, 3),
('BIOS-1000', 'Current Protocals Essential Labratory Te', '2nd', '9780470089934', 'Sean Gallagher', 'Wiley', 112.5, 9),
('BUSI-1010', 'Human Resource Management and Premium Co', '*', '9781118582800', 'Stewart, Belcourt', 'Nelson', 79.5, 30),
('BUSI-2030', 'Understanding Economics: A Contemporary ', '7th', '9781259030802', 'Mark Lovewell', 'McGraw', 122.5, 11),
('BUSI-1010', 'Managerial Accounting', '10th', '9781259066818', 'Garrison', 'McGraw', 148.5, 5),
('ACCT-2000', 'Financial Accounting III - Custom', '2015', '9781259274312', '*', 'McGraw', 112.5, 2),
('COMP-1000', 'Pearson Custom Program with MyITLab', '*', '978126946575', 'Custom', 'Pearson', 153.25, 12),
('ACCT-2005', 'Financial Accounting I & II Custom', '1st', '9781269756549', 'Custom', 'Pearson', 204.5, 18),
('ACCT-1015', 'Introduction to Federal Income Taxation ', '36th', '9781554968060', 'CCH', 'Wolters Kluwer', 119.5, 4),
('ACCT-2005', 'Wizard Inc.', '*', '9781894257848', '*', 'PLP Publishing', 59, 8),
('BUSI-2040', 'The Graphics Company', '*', '9781896321551', '*', 'Blue Sky', 42, 2),
('MATH-1000', 'Mathematics of Business and Finance', '2nd', '9781927737026', 'Daisley,Kugathasa', 'Vretta', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE IF NOT EXISTS `classes` (
  `classCode` varchar(10) NOT NULL DEFAULT '',
  `programCode` varchar(10) DEFAULT NULL,
  `className` varchar(100) DEFAULT NULL,
  `year` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`classCode`, `programCode`, `className`, `year`) VALUES
('ACCT-1000', 'ACCT', 'Financial Accounting I', 1),
('ACCT-1010', 'ACCT', 'Computerized Accounting I', 1),
('ACCT-1015', 'ACCT', 'Personal Taxation', 1),
('ACCT-2000', 'ACCT', 'Financial Accounting III', 2),
('ACCT-2005', 'ACCT', 'Cost Accounting', 2),
('ACCT-2010', 'ACCT', 'Computerized Accounting II', 2),
('ACCT-2015', 'ACCT', 'Corporate Taxation', 2),
('BIOL-1300', 'BIOT', 'Microbiology', 1),
('BIOL-1310', 'BIOT', 'Immunology', 1),
('BIOL-1320', 'BIOT', 'Genetics', 1),
('BIOL-2300', 'BIOT', 'Cell Culturing', 2),
('BIOL-2310', 'BIOT', 'Molecular Biology', 2),
('BIOS-1000', 'BIOT', 'Lab Skills:Bioscience Technology', 1),
('BIOS-1100', 'BIOT', 'Plant Biotechnology', 1),
('BIOS-2000', 'BIOT', 'Analytical Techniques in Bioscience', 2),
('BIOS-2010', 'BIOT', 'Ethics and Professional Practice', 2),
('BIOS-2110', 'BIOT', 'Industrial Processes', 2),
('BIOS-2300', 'BIOT', 'Research Preparation: Bioscience Technology', 2),
('BIOS-2310', 'BIOT', 'Research Project Bioscience Technology', 2),
('BUSI-1000', 'ACCT', 'Business Law', 1),
('BUSI-1010', 'ACCT', 'Human Resource Management I', 1),
('BUSI-2020', 'ACCT', 'Financial Management', 2),
('BUSI-2030', 'ACCT', 'Economics', 2),
('BUSI-2040', 'ACCT', 'Information Management', 2),
('CHEM-1000', 'BIOT', 'General Chemistry', 1),
('CHEM-1200', 'BIOT', 'Introduction to Chromatography', 1),
('CHEM-1300', 'BIOT', 'Biochemistry', 1),
('CHEM-2300', 'BIOT', 'Advanced Biochemistry', 2),
('COMM-1010', 'ACCT', 'Written Communications', 1),
('COMM-1020', 'ACCT', 'Oral Communications', 1),
('COMM-1110', 'BIOT', 'Communications in the Applied Sciences', 1),
('COMP-1000', 'BIOT', 'Computer Essentials', 1),
('COMP-1500', 'ACCT', 'Management Information Systems I', 1),
('ENGL-1000', 'ACCT', 'English', 1),
('MATH-1000', 'ACCT', 'Business Math', 1),
('MATH-1200', 'BIOT', 'Natural Science Math', 1),
('MATH-1300', 'BIOT', 'Statistic', 1),
('MATH-2000', 'BIOT', 'Calculus', 2),
('NCPR-1020', 'BIOT', 'Canadian Council on Animal Care Certification', 1),
('PRAC-2011', 'ACCT', 'On-The-Job Training', 2),
('PRAC-2400', 'BIOT', 'On-The-Job Training: Bioscience Technology', 2),
('SAFE-1005', 'ACCT', 'PEI Occupational Health and Safety', 1);

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE IF NOT EXISTS `programs` (
  `programName` varchar(100) DEFAULT NULL,
  `programCode` varchar(10) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`programName`, `programCode`) VALUES
('Accounting Technology', 'ACCT'),
('Bioscience', 'BIOT'),
('Business Administration', 'BUSA');

-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS `programclasses` (
`id` int(3) NOT NULL COMMENT 'This is the primary key for programclasses',
  `classCode` varchar(30) DEFAULT NULL,
  `programCode` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `programclasses`
--

INSERT INTO `programclasses` (`id`, `classCode`, `programCode`) VALUES
(9, 'ACCT-1000', 'ACCT'),
(10, 'ACCT-1010', 'ACCT'),
(11, 'ACCT-1015', 'ACCT'),
(12, 'ACCT-2000', 'ACCT'),
(13, 'ACCT-2005', 'ACCT'),
(14, 'ACCT-2010', 'ACCT'),
(15, 'ACCT-2015', 'ACCT'),
(16, 'BIOL-1300', 'BIOT'),
(17, 'BIOL-1310', 'BIOT'),
(18, 'BIOL-1320', 'BIOT'),
(19, 'BIOL-2300', 'BIOT'),
(20, 'BIOL-2310', 'BIOT'),
(21, 'BIOS-1000', 'BIOT'),
(22, 'BIOS-1100', 'BIOT'),
(23, 'BIOS-2000', 'BIOT'),
(24, 'BIOS-2010', 'BIOT'),
(25, 'BIOS-2110', 'BIOT'),
(26, 'BIOS-2300', 'BIOT'),
(27, 'BIOS-2310', 'BIOT'),
(28, 'BUSI-1000', 'ACCT'),
(29, 'BUSI-1010', 'ACCT'),
(30, 'BUSI-2020', 'ACCT'),
(31, 'BUSI-2030', 'ACCT'),
(32, 'BUSI-2040', 'ACCT'),
(33, 'CHEM-1000', 'BIOT'),
(34, 'CHEM-1200', 'BIOT'),
(35, 'CHEM-1300', 'BIOT'),
(36, 'CHEM-2300', 'BIOT'),
(37, 'COMM-1010', 'ACCT'),
(38, 'COMM-1020', 'ACCT'),
(39, 'COMM-1110', 'BIOT'),
(40, 'COMP-1000', 'BIOT'),
(41, 'COMP-1500', 'ACCT'),
(42, 'ENGL-1000', 'ACCT'),
(43, 'MATH-1000', 'ACCT'),
(44, 'MATH-1200', 'BIOT'),
(45, 'MATH-1300', 'BIOT'),
(46, 'MATH-2000', 'BIOT'),
(47, 'NCPR-1020', 'BIOT'),
(48, 'PRAC-2011', 'ACCT'),
(49, 'PRAC-2400', 'BIOT'),
(50, 'SAFE-1005', 'ACCT');
--
-- Table structure for table `users`
--
CREATE TABLE IF NOT EXISTS `UserAccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL,
  `createdDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `UserAccess`
--

INSERT INTO `UserAccess` (`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) VALUES
(1, 'user', '202cb962ac59075b964b07152d234b70', 1, '2015-12-01 00:00:00');

INSERT INTO `UserAccess` (`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) VALUES
(1, 'student', '202cb962ac59075b964b07152d234b70', 2, '2015-12-01 00:00:00');

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`ISBN`), ADD KEY `classCode` (`classCode`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`classCode`), ADD KEY `programCode` (`programCode`);

--
-- Indexes for table `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`programCode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- -- Constraints for table `books`
-- --
-- ALTER TABLE `books`
-- ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`classCode`) REFERENCES `classes` (`classCode`);
-- 
-- --
-- -- Constraints for table `classes`
-- --
-- ALTER TABLE `classes`
-- ADD CONSTRAINT `classes_ibfk_1` FOREIGN KEY (`programCode`) REFERENCES `programs` (`programCode`);

/*create a user in database*/
grant select, insert, update, delete on cis2232_booktique.*
             to 'cis2232_admin'@'localhost'
             identified by 'Test1234';
flush privileges;